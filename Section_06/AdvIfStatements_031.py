age = int(input("How old are you? "))

if (age >= 16) and (age <= 65):
    print("Have a good day at work")

if 16 <= age <= 65:
    print("Have an ok day at work")

if 15 < age < 66:
    print("Have a reasonable day at work")

if (age < 16) or (age > 65):
    print("Enjoy your free time")
else:
    print("Have a good day at work")

# -------------------------------
# Bool Class - True / False

print(type(True))

x = "false"
if x:
    print("x is true")  # will print x is true as x value of x is thruthy

y = False
if y:
    print("y is true")
else:
    print("y is false")


x = input("Please enter some text: ")
if x:
    print(f"You entered {x}")
else:
    print("You did not enter anything")

# -------------------------------
print(not False)    # prints True
print(not True)     # prints False
# -------------------------------
# Using keyword 'not'

age = int(input("How old are you?"))
if not(age < 18):
    print("You are old enough to vote")
    print("Please put an X in the box")
else:
    print(f"Please come back in {18 - age} years")

# -------------------------------
# Using 'in' keyword

parrot = "Norwegian Blue"
letter = input("Enter a character: ")

if letter in parrot:
    print(f"Give me an {letter}, Bob.")
else:
    print("I don't need that letter")

# Note the 'in' check is case sensitive, so an upper or lower call could be used to do case insensitive matches
