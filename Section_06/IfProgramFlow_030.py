name = input("Please enter your name: ")
age = int(input(f"How old are you, {name}? "))

print(age)

if age >= 18:
    print("You are old enough to vote")
    print("Please put and X in the box")
else:
    print(f"Please come back in {18 - age} years")

print("------------------------------")


print("Please guess a number between 1 and 10: ")
guess = int(input())

# very ugly code
# if guess < 5:
#     print("Please guess higher")
#     guess = int(input())
#     if guess == 5:
#         print("Well done, you guessed it")
#     else:
#         print("Sorry, you have not guessed correctly")
# elif guess > 5:
#     print("Please guess lower")
#     guess = int(input())
#     if guess == 5:
#         print("Well done, you guessed it")
#     else:
#         print("Sorry, you have not guessed correctly")
# else:
#     print("You got it first time")

# slightly less ugly code
if guess != 5:
    if guess < 5:
        print("Please guess higher.")
    else:
        print("Please guess lower.")

    guess = int(input())

    if guess == 5:
        print("Well done, you guessed it")
    else:
        print("Sorry, you have not guessed correctly")
else:
    print("You got it first time")