import random

# Modify the program below to use a while loop to
# allow as many guesses as necessary.

# The program should let the player know whether to
# guess higher or lower, and should print a message
# when the guess is correct. A correct guess will
# terminate the program

# As an optional extra, allow the player to quit by
# entering 0 (zero) for their guess


minValue = 1
maxValue = 100

# Generate a random answer
answer = random.randint(minValue, maxValue)

print(f"Please guess a number between {minValue} and {maxValue} :")
guess = int(input())

while guess != answer:

    # catch the exit condition
    if guess == 0:
        print("Thanks for playing!")
        break

    if guess < answer:
        print("Please guess higher")
    else:
        print("Please guess lower")

    guess = int(input())

if guess == answer:
    print("Well done, you guessed it")
