shopping_list = ["milk", "pasta", "eggs", "spam", "bread", "rice"]

for item in shopping_list:
    if item == "spam":
        continue    # don't print out spam, skip processing for this iteration
    print(f"Buy {item}")

print("-----------")

for item in shopping_list:
    if item == "spam":
        break   # abort the loop entirely if spam found
    print(f"Buy {item}")

print("-----------")

meal = ["egg", "bacon", "spamm", "sausages"]
nasty_food = ''

for item in meal:
    if item == 'spam':
        nasty_food = item
        break
else:   # python feature, gets executed if the for loop completes
    print("I'll have a plate of that then please")

if nasty_food:
    print("Can't I have anything without spam in it")
