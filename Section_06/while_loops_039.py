import random

for i in range(10):
    print(f"i is now {i}")


i = 0
while i < 10:
    print(f"i is now {i}")
    i += 1


# availableExits = ["east", "north-east", "south"]
#
# chosenExit = ""
# while chosenExit not in availableExits:
#     chosenExit = input("Please choose a direction: ")
#     if chosenExit == "Quit":
#         print("Game Over")
#         break
# else:
#     print("aren't you glad you got out of there!")

# -------------------------------------------

highest = 10
answer = random.randint(1, highest)

print(f"Please guess a number between 1 and {highest}")
guess = int(input())

if guess != answer:
    if guess < answer:
        print("Please guess higher")
    else:
        print("Please guess lower")

    guess = int(input())

    if guess == answer:
        print("Well done, you guessed it")
    else:
        print("Sorry, you have not guessed correctly")
else:
    print("You got it first time")
