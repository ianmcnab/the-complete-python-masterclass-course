# all 3 lines in the same code block due to indentation
# as python doesn't use brackets for block level delimiting
# typically 4 spaces per indent or PEP8 will grumble at you.

for i in range(1, 12):
    print("No {:2} squared is {:4} and cubed is {:4}".format(i, i**2, i**3))
    print("Calculation complete")
    print("Try again")

print("This is after the for loop completes")