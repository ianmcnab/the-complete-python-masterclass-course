for i in range(1, 20):
    print(f"i is now {i}")

number = "9,223,372,036,854,775,807"
cleanedNumber = ""

for i in range(0, len(number)):
    if number[i] in '0123456789':
        print(number[i], end='')
        cleanedNumber += number[i]

print("\n-----------------")
print(cleanedNumber)
x = int(cleanedNumber)
print(x+1)
