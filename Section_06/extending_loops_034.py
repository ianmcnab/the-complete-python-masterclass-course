number = "9,223,372,036,854,775,807"
cleanedNumber = ""

for char in number:
    if char in '0123456789':
        cleanedNumber += char

newNumber = int(cleanedNumber)
print(f"The number is: {newNumber}")

print("--------------------")

for state in ["not pinin'", "no more", "a stiff", "bereft of life"]:
    print(f"This parrot is {state}")

print("--------------------")

for i in range(0, 100, 5):
    print(f"i is {i}")


for i in range(1, 13):
    for j in range(1, 13):
        print(f"{i:2} x {j:2} = {i * j:3}", end='\t')
    print('')     # carriage return at the end of 12th column

