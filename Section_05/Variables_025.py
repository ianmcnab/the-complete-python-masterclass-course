greeting = "Bruce"

# underscores are valid (typically private variables)
_myname = "Tim"

# this is invalid
# 1Tim = "Good"

Tim45 = "Good"
Tim_was_57 = "hello"

# note case sensitivity  ie Greeting != greeting
Greeting = "There"

print(Tim_was_57 + ' ' + greeting)

age = 24
print(age)

# Cannot print ints directly they need to be cast to strings first
# print(greeting + age)
print("Age is type:\t\t" + str(type(age)))          # class int
print("greeting is type:\t" + str(type(greeting)))  # class string

# Also note that type is of type 'type' so also needs cast to a string for the above printing
# unless printed out by itself without the string
print(type(type(age)))

# use of end='' is a feature of python 3 which defaults a \n to strings, the use of '' denotes an empty
# string instead which allows for line continuation and so the type is shown on the same line
# as we are not operator overloading using '+' and using two separate print statements we don't need to cast
# the type to a string.
print("Type is of type: ", end='')
print(type(type))

print(greeting + str(age))

print("----------------------------------------")
print("Integers")
print("--------")

a = 11   # Int
b = 3
print(a + b)        # addition
print(a - b)        # subtraction
print(a * b)        # multiplication
print(a / b)        # standard division (python2 uses this as rounded division) will produce a float
print(a // b)       # rounded division (rounded down python 2 fallback behaviour)
print(a % b)        # modulus (ie division remainder)
print("--------")

# Obviously in a for loop the range must be an int, floats don't make sense here
for i in range(1, a):
    print(i)

a = 12

# standard order of operations for maths
print(a + b / 3 - 4 * 12)
# same as:
print(a + (b / 3) - (4 * 12))
