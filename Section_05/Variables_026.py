a = 12   # Int
b = 3
print(a + (b / 3) - (4 * 12))

c = a + b
d = c / 3
e = d - 4
print(e * 12)

parrot = "Norwegian Blue"
print(parrot)
# zero indexed as normal
print(parrot[3])

# count backwards, -1 gives the last letter
print(parrot[-1])
# Gives the first
print(parrot[-14])
# Gives an out of range syntax error
# print(parrot[-15])

# slices
print(parrot[0:6])
# and reversed
print(parrot[5::-1])


# from position to end
print(parrot[6:])
print(parrot[-4:-2])
# stepping by 2
print(parrot[0:6:2])
print(parrot[0:6:3])

number= "9,223,372,036,854,775,807"
# start at offset 1, get every 4th character eg the commas
print(number[1::4])


numbers = "1, 2, 3, 4, 5, 6, 7, 8, 9"
# remove the ', '
print(numbers[0::3])
string1 = "he's "
string2 = "probably"
print(string1 + string2)
# + operator not required
print("he's " "probably " "pining")

print("Hello " * 5)
print("Hello " * (5 + 4))
print("Hello " * 5 + "4")

today = "friday"
# boolean check for substring using 'in' keyword
print("day" in today)
print("fri" in today)
print("thur" in today)
print("parrot" in "fjord")
