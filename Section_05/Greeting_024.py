greeting = "Hello"
# Input takes user input from keyboard
name = input("Please enter your name: ")
print(greeting + " " + name)
print("---------------------------")

splitString = "This string has been\nsplit over\nseveral\nlines"
print(splitString)
print("---------------------------")

tabbedString = "1\t2\t3\t4\t5\t"
print(tabbedString)
print("---------------------------")

# You can also avoid ending string quotes by escaping them with a backslash
print('The pet shop owner said "No, no, \'e\'s uh, ... he\'s resting"')
# Same this with double quotes instead
print("The pet shop owner said \"No, no, 'e's uh, ... he's resting\"")

print("---------------------------")

anotherSplitString = """1. This string has been
split over
several lines"""
print(anotherSplitString)

# Note the spacing impacts in the output as the first line
# is hard left aligned the others are offset so the white space matters
anotherSplitString2 = """2. This string has been
                         split over
                         several lines"""
print(anotherSplitString2)

# Note fixes the alignment but DOES include the opening blank line
anotherSplitString3 = """
                         3.This string has been
                         split over
                         several lines"""
print(anotherSplitString3)

# To avoid this terminate the first line with a backslash
anotherSplitString4 = """\
                         4.This string has been
                         split over
                         several lines"""
print(anotherSplitString4)
print("---------------------------")

# Now with triple quotes using doubles, note that a space is required between the last quote and
# the closing triple quote or the interpreter gets confused
print("""The pet shop owner said "No, no, 'e's uh, ... he's resting" """)
# or singles...
print('''The pet shop owner said "No, no, 'e's uh, ... he's resting" ''')
