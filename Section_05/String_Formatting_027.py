age = 24
# This won't work as int needs to be cast to a string first
# print("My age is " + age + " years")

# cast age to a string
print("My age is " + str(age) + " years")

# using an f string instead
print(f"My age is {age} years")

# another way
print("My age is {0} years".format(age))

print("There are {0} days in {1}, {2}, {3}, {4}, {5}, {6} and {7}".format(31, "January", "March", "May",
                                                                          "July", "August", "October", "December"))

# Reuse of arguments
print("""January:\t{2}
February:\t{0}
March:\t\t{2}
April:\t\t{1}
May:\t\t{2}
June:\t\t{1}
July:\t\t{2}
August:\t\t{2}
September:\t{1}
October:\t{2}
November:\t{1}
December:\t{2}""".format(28, 30, 31))

# deprecated format
print("My age is %d years" % age)
print("My age is %d %s, %d %s" % (age, "years", 6, "months"))

# old format
for i in range(1, 12):
    print("No. %2d squared is %4d and cubed is %4d" % (i, i**2, i**3))

# old format
print("Pi is approximately %12.50f" % (22/7))

# new format
for i in range(1, 12):
    print("No. {0:2} squared is {1:4} and cubed is {2:4}".format(i, i**2, i**3))

# new format, using < to left justify
for i in range(1, 12):
    print("No. {0:2} squared is {1:<4} and cubed is {2:<4}".format(i, i**2, i**3))

# new format
print("Pi is approximately {0:12.50}".format(22/7))

# new format, inferred positions
for i in range(1, 12):
    print("No. {:2} squared is {:<4} and cubed is {:<4}".format(i, i**2, i**3))
